package br.com.builders.treinamento.dto.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class TestValidationResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String field;
	private String error;

	public TestValidationResponse() {
		super();
	}

	public TestValidationResponse(String field, String error) {
		super();
		this.field = field;
		this.error = error;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
