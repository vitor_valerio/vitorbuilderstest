package br.com.builders.treinamento.dto.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@ApiModel(value = "CustomerRequest")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class CustomerRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String crmId;
	private String baseUrl;
	private String name;
	private String login;

}
