package br.com.builders.treinamento.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.builders.treinamento.domain.Customer;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

	Customer getByLogin(String login);

}
