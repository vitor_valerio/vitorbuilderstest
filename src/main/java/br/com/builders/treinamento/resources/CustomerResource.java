package br.com.builders.treinamento.resources;

import java.util.ArrayList;
import java.util.List;

import javax.naming.ServiceUnavailableException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.response.TestValidationResponse;
import br.com.builders.treinamento.exception.ErrorCodes;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.RecordAlreadyRegisteredException;
import br.com.builders.treinamento.service.CustomerService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api")
@Api(value = "API Customer")
public class CustomerResource {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/customers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<List<Customer>> findAll() throws ServiceUnavailableException {
		List<Customer> result = customerService.findAll();
		return new ResponseEntity<List<Customer>>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/customers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Void> insert(@RequestBody Customer customer)
			throws ServiceUnavailableException, RecordAlreadyRegisteredException {
		customerService.insert(customer);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/customers/{idCustomer}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Customer> get(@PathVariable("idCustomer") String idCustomer)
			throws ServiceUnavailableException, NotFoundException {
		Customer customer = customerService.get(idCustomer);
		if (customer == null) {
			throw new NotFoundException(messageSource.getMessage(ErrorCodes.RECORD_NOT_FOUND, new String[] {}, null));
		}
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@RequestMapping(value = "/customers/{idCustomer}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Void> update(@PathVariable("idCustomer") String idCustomer,
			@RequestBody Customer customer) throws ServiceUnavailableException, NotFoundException {
		customer.setId(idCustomer);
		customerService.update(customer);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/customers/{idCustomer}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Void> modify(@PathVariable("idCustomer") String idCustomer,
			@RequestBody Customer customer) throws ServiceUnavailableException, NotFoundException {
		customer.setId(idCustomer);
		customerService.modify(customer);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/customers/{idCustomer}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Void> delete(@PathVariable("idCustomer") String idCustomer) throws ServiceUnavailableException, NotFoundException {
		customerService.remove(idCustomer);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ResponseEntity<List<TestValidationResponse>> handleExceptionValidation(ConstraintViolationException e) {
		List<TestValidationResponse> errors = new ArrayList<>();
		e.getConstraintViolations().forEach(constraintViolation -> {
			errors.add(TestValidationResponse.builder().field(constraintViolation.getPropertyPath().toString())
					.error(constraintViolation.getMessage()).build());
		});
		return new ResponseEntity<>(errors, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseEntity<String> handleExceptionNotFound(NotFoundException ex) {
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(RecordAlreadyRegisteredException.class)
	@ResponseStatus(value = HttpStatus.CONFLICT)
	public ResponseEntity<TestValidationResponse> handleExceptionAlreadyExists(RecordAlreadyRegisteredException ex) {
		return new ResponseEntity<>(
				TestValidationResponse.builder().field(ex.getField()).error(ex.getMessage()).build(), new HttpHeaders(),
				HttpStatus.CONFLICT);
	}

}
