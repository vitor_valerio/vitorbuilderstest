package br.com.builders.treinamento.service.impl;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.enumeration.TestValidationContext;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.RecordAlreadyRegisteredException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final String LOGIN_FIELD = "login";

	private static final String ID_FIELD = "id";

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private Validator validator;

	@Value("${validateRecord.already.registered}")
	private String recordAlreadyRegistered;

	@Value("${validateRecord.invalid.notFound}")
	private String recordNotFound;

	@Override
	public Customer insert(Customer customer) throws RecordAlreadyRegisteredException {
		validatePreInsert(customer);
		return customerRepository.insert(customer);
	}

	@Override
	public void update(Customer customer) throws NotFoundException {
		validatePreUpdate(customer);
		customerRepository.save(customer);
	}

	@Override
	public void modify(Customer customer) throws NotFoundException {
		validatePreModify(customer);
		Customer savedCustomer = get(customer.getId());
		savedCustomer.setBaseUrl(customer.getBaseUrl() != null ? customer.getBaseUrl() : savedCustomer.getBaseUrl());
		savedCustomer.setCrmId(customer.getCrmId() != null ? customer.getCrmId() : savedCustomer.getCrmId());
		savedCustomer.setLogin(customer.getLogin() != null ? customer.getLogin() : savedCustomer.getLogin());
		savedCustomer.setName(customer.getName() != null ? customer.getName() : savedCustomer.getName());
	}

	@Override
	public void remove(String id) throws NotFoundException {
		validatePreRemove(id);
		customerRepository.delete(id);
	}

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public Customer get(String id) {
		return customerRepository.findOne(id);
	}

	private void validatePreModify(Customer customer) throws NotFoundException {
		validateExists(customer);
		Set<ConstraintViolation<Customer>> constraintViolations = validator.validateProperty(customer, "login", Default.class);

		if (!constraintViolations.isEmpty()) {
			throw new ConstraintViolationException(constraintViolations);
		}
	}

	private void processValidate(Customer customer, TestValidationContext context) {
		Set<ConstraintViolation<Customer>> constraintViolations = validator.validate(customer);

		if (!constraintViolations.isEmpty()) {
			throw new ConstraintViolationException(constraintViolations);
		}
	}

	private void validatePreInsert(Customer customer) throws RecordAlreadyRegisteredException {
		processValidate(customer, TestValidationContext.INSERT);

		Customer aux = customerRepository.findOne(customer.getId());

		if (aux != null) {
			throw new RecordAlreadyRegisteredException(recordAlreadyRegistered, ID_FIELD);
		}

		aux = customerRepository.getByLogin(customer.getLogin());

		if (aux != null) {
			throw new RecordAlreadyRegisteredException(recordAlreadyRegistered, LOGIN_FIELD);
		}
	}

	private void validatePreUpdate(Customer customer) throws NotFoundException {
		processValidate(customer, TestValidationContext.UPDATE);

		validateExists(customer);
	}

	private void validateExists(Customer customer) throws NotFoundException {
		Customer aux = customerRepository.findOne(customer.getId());

		if (aux == null) {
			throw new NotFoundException(recordNotFound);
		}
	}

	// Validate if the document is used in another collection
	private void validatePreRemove(String id) throws NotFoundException {
		Customer aux = Customer.builder().id(id).build();

		validateExists(aux);
	}


}
