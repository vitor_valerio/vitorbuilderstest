package br.com.builders.treinamento.service;

import java.util.List;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.RecordAlreadyRegisteredException;

public interface CustomerService {

	Customer insert(Customer customer) throws RecordAlreadyRegisteredException;

	void update(Customer customer) throws NotFoundException;

	void modify(Customer customer) throws NotFoundException;

	void remove(String id) throws NotFoundException;

	List<Customer> findAll();

	Customer get(String id);

}
