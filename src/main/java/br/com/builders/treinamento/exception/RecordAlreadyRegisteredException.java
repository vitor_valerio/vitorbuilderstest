/*
* Copyright 2018 Builders
*************************************************************
*Nome     : NotFoundException.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;

public class RecordAlreadyRegisteredException extends APIException {

	private static final long serialVersionUID = 1L;
	private String field;
	private String message;

	public RecordAlreadyRegisteredException(final Throwable cause) {
		super(HttpStatus.BAD_REQUEST, cause);
	}

	public RecordAlreadyRegisteredException(final String msg, final String field) {
		super(HttpStatus.BAD_REQUEST, new Throwable(msg));
		this.field = field;
		this.message = msg;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
