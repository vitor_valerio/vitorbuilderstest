package br.com.builders.treinamento.domain;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import lombok.Builder;
import lombok.ToString;

@ToString
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@Builder
public class Customer implements Serializable {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 8727605660588027466L;

	@Id
	private String id;
	@NotEmpty(message = "{validateField.required}")
	private String crmId;
	@NotEmpty(message = "{validateField.required}")
	private String baseUrl;
	@NotEmpty(message = "{validateField.required}")
	private String name;
	@Email(message = "{validateField.invalid.email}")
	@NotEmpty(message = "{validateField.required}")
	private String login;

	public Customer() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Customer(String id, String crmId, String baseUrl, String name, String login) {
		super();
		this.id = id;
		this.crmId = crmId;
		this.baseUrl = baseUrl;
		this.name = name;
		this.login = login;
	}

}
