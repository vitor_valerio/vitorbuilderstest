/*
* Copyright 2018 Builders
*************************************************************
*Nome     : Slf4SampleImpl.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.logger.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.builders.treinamento.logger.Slf4Sample;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Slf4SampleImpl extends Slf4Sample {

	private static Logger LOGGER = LoggerFactory.getLogger(Slf4SampleImpl.class);

	@Override
	protected void SampleMethod(String id, String nono) {
		LOGGER.info("Describe message: id={}, nono={}.", id, nono);
	}

}
