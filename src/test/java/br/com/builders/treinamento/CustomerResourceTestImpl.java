package br.com.builders.treinamento;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.response.TestValidationResponse;
import br.com.builders.treinamento.exception.RecordAlreadyRegisteredException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerResourceTestImpl {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private static final String NOT_FOUND_ID = "NOT_FOUND_ID_2805181751";

	private static final String NAME_UPDATED = "name_updated";

	private static final String VALID_DIFERENT_ID = "valid_diferent_id";

	private static final String VALID_DIFERENT_EMAIL = "valid_diferent_email@gmail.com";

	private static final String SUCCESS_ID_CONSTANT = "vitor_success_Id";

	private static final String SUCCESS_LOGIN_CONSTANT = "vitor_success_login@gmail.com";

	private static final String INVALID_EMAIL_FORMAT = "invalidemailformat";

	private String url;

	@PostConstruct
	public void init() {
		StringBuilder sb = new StringBuilder("http://localhost:").append(port).append("/api/customers");
		url = sb.toString();
	}

	@Test
	public void testAValidateInsertSuccess() throws RecordAlreadyRegisteredException, JsonProcessingException {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, SUCCESS_LOGIN_CONSTANT);
		ResponseEntity<Void> response = restTemplate.postForEntity(url, customer, Void.class);
		Assert.assertTrue(response.getStatusCode().equals(HttpStatus.CREATED));
	}

	@Test
	public void testBValidateInsertRequiredFields() throws Exception {
		Customer customer = new Customer();
		ResponseEntity<TestValidationResponse[]> response = restTemplate.postForEntity(url, customer,
				TestValidationResponse[].class);
		Assert.assertTrue(response.getStatusCode().equals(HttpStatus.BAD_REQUEST));
		List<String> list = Arrays.asList(response.getBody()).stream().map(TestValidationResponse::getField).collect(Collectors.toList());
		Assert.assertTrue("Contains baseUrl error", list.contains("baseUrl"));
		Assert.assertTrue("Contains crmId error", list.contains("crmId"));
		Assert.assertTrue("Contains login error", list.contains("login"));
		Assert.assertTrue("Contains name error", list.contains("name"));
	}

	@Test
	public void testCValidateInsertEmailInvalid() throws Exception {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, INVALID_EMAIL_FORMAT);
		ResponseEntity<TestValidationResponse[]> response = restTemplate.postForEntity(url, customer,
				TestValidationResponse[].class);
		Assert.assertTrue(response.getStatusCode().equals(HttpStatus.BAD_REQUEST));
		List<String> list = Arrays.asList(response.getBody()).stream().map(TestValidationResponse::getField).collect(Collectors.toList());
		Assert.assertTrue("Only login error returned", list.size() == 1);
		Assert.assertTrue("Contains login error", list.contains("login"));
	}

	@Test
	public void testDValidateInsertIdAlreadyExists() throws RecordAlreadyRegisteredException {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, VALID_DIFERENT_EMAIL);
		ResponseEntity<TestValidationResponse> response = restTemplate.postForEntity(url, customer,
				TestValidationResponse.class);
		Assert.assertTrue(response.getStatusCode().equals(HttpStatus.CONFLICT));
		Assert.assertTrue(response.getBody().getField().equals("id"));
	}

	@Test
	public void testEValidateInsertLoginAlreadyExists() throws RecordAlreadyRegisteredException {
		Customer customer = createDefaultCustomer(VALID_DIFERENT_ID, SUCCESS_LOGIN_CONSTANT);
		ResponseEntity<TestValidationResponse> response = restTemplate.postForEntity(url, customer,
				TestValidationResponse.class);
		Assert.assertTrue(response.getStatusCode().equals(HttpStatus.CONFLICT));
		Assert.assertTrue(response.getBody().getField().equals("login"));
	}

	@Test
	public void testFValidateUpdateSuccess() {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, SUCCESS_LOGIN_CONSTANT);
		customer.setName(NAME_UPDATED);

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers); 

		ResponseEntity<Void> response = restTemplate.exchange(url + "/" + SUCCESS_ID_CONSTANT, HttpMethod.PUT, entity, Void.class);

		Assert.assertTrue("Update success returned", HttpStatus.OK.equals(response.getStatusCode()));

		ResponseEntity<Customer> updatedCustomer = restTemplate.getForEntity(url + "/" + SUCCESS_ID_CONSTANT, Customer.class);
		Assert.assertTrue("Record's been updated", NAME_UPDATED.equals(updatedCustomer.getBody().getName()));
	}

	@Test
	public void testGValidateUpdateNotFound() {
		Customer customer = createDefaultCustomer(NOT_FOUND_ID, SUCCESS_LOGIN_CONSTANT);
		customer.setName(NAME_UPDATED);

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers); 

		ResponseEntity<Void> response = restTemplate.exchange(url + "/" + NOT_FOUND_ID, HttpMethod.PUT, entity, Void.class);

		Assert.assertTrue("Update not found returned", HttpStatus.NOT_FOUND.equals(response.getStatusCode()));
	}

	@Test
	public void testHValidateUpdateRequiredFields() {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, SUCCESS_LOGIN_CONSTANT);
		customer.setBaseUrl("");
		customer.setCrmId("");
		customer.setLogin("");
		customer.setName("");

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers); 

		ResponseEntity<TestValidationResponse[]> response = restTemplate.exchange(url + "/" + SUCCESS_ID_CONSTANT, HttpMethod.PUT, entity, TestValidationResponse[].class);

		Assert.assertTrue("Update validation error", HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));

		List<String> list = Arrays.asList(response.getBody()).stream().map(TestValidationResponse::getField).collect(Collectors.toList());
		Assert.assertTrue("Contains baseUrl error", list.contains("baseUrl"));
		Assert.assertTrue("Contains crmId error", list.contains("crmId"));
		Assert.assertTrue("Contains login error", list.contains("login"));
		Assert.assertTrue("Contains name error", list.contains("name"));

	}

	@Test
	public void testIValidateUpdateEmailValid() {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, SUCCESS_LOGIN_CONSTANT);
		customer.setLogin(INVALID_EMAIL_FORMAT);

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers); 

		ResponseEntity<TestValidationResponse[]> response = restTemplate.exchange(url + "/" + SUCCESS_ID_CONSTANT, HttpMethod.PUT, entity, TestValidationResponse[].class);

		Assert.assertTrue("Update email validation error", HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
		List<String> list = Arrays.asList(response.getBody()).stream().map(TestValidationResponse::getField).collect(Collectors.toList());
		Assert.assertTrue("Only login error returned", list.size() == 1);
		Assert.assertTrue("Contains login error", list.contains("login"));
	}

	@Test
	public void testJValidatePatchSuccess() {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, SUCCESS_LOGIN_CONSTANT);
		customer.setName(NAME_UPDATED);

		HttpHeaders headers = new HttpHeaders();
		MediaType mediaType = new MediaType("application", "merge-patch+json");
		headers.setContentType(mediaType);

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers);
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		RestTemplate restTemplate = new RestTemplate(requestFactory);

		ResponseEntity<Void> response = restTemplate.exchange(url + "/" + SUCCESS_ID_CONSTANT, HttpMethod.PATCH, entity, Void.class);

		Assert.assertTrue("Patch success returned", HttpStatus.OK.equals(response.getStatusCode()));

		ResponseEntity<Customer> updatedCustomer = restTemplate.getForEntity(url + "/" + SUCCESS_ID_CONSTANT, Customer.class);
		Assert.assertTrue("Record's been updated", NAME_UPDATED.equals(updatedCustomer.getBody().getName()));
	}

	@Test
	public void testLValidatePatchEmailValid() {
		Customer customer = createDefaultCustomer(SUCCESS_ID_CONSTANT, SUCCESS_LOGIN_CONSTANT);
		customer.setLogin(INVALID_EMAIL_FORMAT);

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers); 

		ResponseEntity<TestValidationResponse[]> response = restTemplate.exchange(url + "/" + SUCCESS_ID_CONSTANT, HttpMethod.PATCH, entity, TestValidationResponse[].class);

		Assert.assertTrue("Patch email validation error", HttpStatus.BAD_REQUEST.equals(response.getStatusCode()));
		List<String> list = Arrays.asList(response.getBody()).stream().map(TestValidationResponse::getField).collect(Collectors.toList());
		Assert.assertTrue("Only login error returned", list.size() == 1);
		Assert.assertTrue("Contains login error", list.contains("login"));
	}

	@Test
	public void testMValidatePatchNotFound() {
		Customer customer = createDefaultCustomer(NOT_FOUND_ID, SUCCESS_LOGIN_CONSTANT);
		customer.setName(NAME_UPDATED);

		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

		HttpEntity<Customer> entity = new HttpEntity<>(customer, headers); 

		ResponseEntity<Void> response = restTemplate.exchange(url + "/" + NOT_FOUND_ID, HttpMethod.PATCH, entity, Void.class);

		Assert.assertTrue("Patch not found returned", HttpStatus.NOT_FOUND.equals(response.getStatusCode()));
	}

	@Test
	public void testNValidateDeleteSuccess() {
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON); 

	    restTemplate.delete(url + "/" + SUCCESS_ID_CONSTANT);

		ResponseEntity<?> responseDeleted = restTemplate.getForEntity(url + "/" + SUCCESS_ID_CONSTANT, Object.class);
		Assert.assertTrue("Customer's been deleted", HttpStatus.NOT_FOUND.equals(responseDeleted.getStatusCode()));
	}

	private Customer createDefaultCustomer(String id, String login) {
		return Customer.builder().id(id).crmId("151515").baseUrl("http://www.platformbuilders.com").name("Builders")
				.login(login).build();
	}

}
